# My Vim Config

## Installation Instructions

### Linux / macOS

```
cd ~
git clone https://github.com/rwoliver2/my-vim-config .rwoliver2-vim-config
ln -s ~/.rwoliver2-vim-config/.vim
ln -s ~/.rwoliver2-vim-config/.vimrc
```

### Windows

1. Download ZIP archive
2. Extract it to `%userprofile%` (or `C:\Users\YOUR_USERNAME`)
3. Rename `.vimrc` to `_vimrc` and `.vim` to `vimfiles`

### All Platforms

Run vim, then hit `ESC` and type `:PlugInstall!` and hit `ENTER`. Your plugins will be installed. Then exit and restart vim and installation will be complete.
